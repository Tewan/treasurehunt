import React from 'react'
import { createStackNavigator } from 'react-navigation-stack'
import { createBottomTabNavigator } from 'react-navigation-tabs'
import { createAppContainer, createSwitchNavigator } from 'react-navigation'
import Icon from 'react-native-vector-icons/FontAwesome5'
import Home from '../screens/Home'
import Login from '../screens/Login'
import SignIn from '../screens/SignIn'
import Loading from '../screens/Loading'
import MapScreen from '../screens/MapScreen'
import MyAccount from '../screens/MyAccount'
import HuntsScreen from '../screens/HuntsScreen'
import {greenColor} from "../style/style"

const AuthStack = createStackNavigator({
    Home: {
        screen: Home,
        navigationOptions: {
            header: null
        }
    },
    Login: {
        screen: Login,
        navigationOptions: {
            title: "Connexion",
            headerTintColor: greenColor
        }
    },
    SignIn: {
        screen: SignIn,
        navigationOptions: {
            title: "S'inscrire",
            headerTintColor: greenColor
        }
    }
});

const TabNav = createBottomTabNavigator({
    HuntsScreen: {
        screen: HuntsScreen,
        navigationOptions: {
            tabBarLabel: "Chasses aux trésors",
            tabBarIcon: ({horizontal, tintColor}) =>
                <Icon name="home" size={horizontal ? 20 : 25}  color={tintColor}/>
        }
    },
    MyAccount: {
        screen: MyAccount,
        navigationOptions: {
            tabBarLabel: "Mon compte",
            tabBarIcon: ({horizontal, tintColor}) =>
            <Icon name="cogs" size={horizontal ? 20 : 25} color={tintColor}/>
        }
    }
}, {
    tabBarOptions: {
        activeTintColor: greenColor,
        inactiveTintColor: 'gray'
    }
});

const MapNav = createStackNavigator({
    MapScreen: {
        screen: MapScreen,
        navigationOptions: {
            header: null
        }
    }
})

const App = createSwitchNavigator({
    Auth: {
        screen: AuthStack
    },
    App: {
        screen: TabNav
    },
    Loading: {
        screen: Loading
    },
    MapNav: {
        screen: MapNav
    }
})

const AppContainer = createAppContainer(App);

export default AppContainer;