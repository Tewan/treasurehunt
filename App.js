import React from 'react';
import AppContainer from './navigation/AppNavigator'
import { ProfileProvider } from './components/ProfileProvider';


export default class App extends React.Component {
  render() {
    return (
      <ProfileProvider> 
        <AppContainer/>
      </ProfileProvider>
    )}
}