import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

export const ButtonIconEdit = ({onPress, style}) => {
    return (
        <TouchableOpacity onPress={onPress} style={style}>
            <Ionicons name="md-create" size={32} color="white"/>
        </TouchableOpacity>
    );
}

export const Button = ({title, style, textStyle, onPress}) => {

    return (
        <TouchableOpacity onPress={onPress} style={[style]}>       
            <Text style={[textStyle]}>{title}</Text>
        </TouchableOpacity>
    );
}

