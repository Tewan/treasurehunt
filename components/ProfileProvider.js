import React from 'react';

export const ProfileContext = React.createContext();

export class ProfileProvider extends React.Component {
    state = {
        __v: null,
        _id: "",
        birthdate: "",
        email: "",
        firstname: "",
        lastname: "",
        password: "",
        selectedHunt: "",

      setUser: (user) => this.setState({
        ...user
      }),

      selectHunt: (id) => this.setState({
        selectedHunt: id
      }),

      removeHuntId: () => this.setState({
        selectedHunt: ""
      })
    }
    render() {
      return (
        <ProfileContext.Provider value={this.state}>
          {this.props.children}
        </ProfileContext.Provider>
      )
    }
  }