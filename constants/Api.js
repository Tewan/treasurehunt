// API de base
export const API = "https://serene-waters-97669.herokuapp.com/api/";
// URI pour se connecter
export const API_LOGIN = API + "login";
// URI pour les utilisateurs
export const API_USERS = API + "users";
// URI pour les chasses
export const API_COURSES = API + "courses";