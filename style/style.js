import { StyleSheet } from 'react-native';
import { AuthSession } from 'expo';

export const greenColor = "#B1C18C";
const greyColor = "#47525E";

export const Style = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    titre: {
        fontSize: 35,
        textAlign: "center",
        color: "white",
        marginTop: '10%',
        fontWeight: "bold"
    },
    backgroundImage: {
        width: '100%',
        height: '100%',
    },
    buttonLogin: {
        borderRadius: 40,
        backgroundColor: greenColor,
        alignItems: 'center',
        marginBottom: '3%',
        paddingVertical: '3%',
        paddingHorizontal: '15%'
    },
    textButtonStyle: {
        color: 'white',
        fontSize: 20
    },
    registerButtonStyle: {
        color: 'black',
        fontSize: 15,
        textAlign: 'center',
        textDecorationLine: 'underline',
    },
    buttonGroup: {
        marginTop: '15%',
    },
    input: {
        borderBottomColor: 'white',
        borderBottomWidth: 1,
        color: 'white',
        marginBottom: '10%',
        paddingLeft: '5%'
    },
    card: {
        backgroundColor: "white",
        width: "85%",
        //height: 360,
        borderRadius: 10,
        marginBottom: 50
    },
    cardNoteTitle: {
        fontSize: 20,
        color: greenColor,
        textAlign: "center"
    },
    cardButton: {
        borderRadius: 5,
        backgroundColor: greenColor,
        alignItems: 'center',
        paddingVertical: 5,
        width: 80
    },

    map: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0
    },

    profilePic: {
        width: 150,
        alignSelf: "center",
        height: 150,
        borderWidth: 2,
        borderColor: greyColor,
        borderRadius: 100
    },

    profileTitle: {
        color: greenColor,
        marginTop: 20,
        fontWeight: "bold"
    },

    iconButtonAccount: {
        position: "absolute",
        top: 10,
        right: 10,
        borderRadius: 100,
        width: 50,
        height: 50,
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: greenColor
    }
});

export const inputColor = "white";