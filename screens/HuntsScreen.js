import React, { Component }  from 'react';
import { SafeAreaView, ScrollView, View, ImageBackground, Text, Image, ActivityIndicator, Dimensions } from 'react-native';

import { Button } from '../components/Button'
import { API_COURSES } from '../constants/Api'
import { Style } from '../style/style'
import { ProfileContext } from '../components/ProfileProvider';

const { height } = Dimensions.get('window');

export default class HuntsScreen extends Component  {

    static contextType = ProfileContext;

    constructor (props) {
        super(props);
        this._isMounted = false;
        this._isLoading = false
        this.state = {
            hunts: [],
        }
    }

    componentDidMount() {
        this._isMounted = true;
        this._isMounted && this._getHunts();
    }

    _getHunts = async () => {
        let res = await fetch(API_COURSES);
        let hunts = await res.json()
        console.log(hunts);
        if(hunts.length > 0) {
            this.setState({hunts});
        }
    }

    /*
    renderIfHunts() {
        if(this.state.isLoading && this.state.hunts.length === 0) {
            _getHunts();
            this.setState({isLoading: false});
            console.log(this.state.hunts);
        }
    }*/

    _renderHunts = function() {
        return this.state.hunts.length > 0 ? this.state.hunts.map((hunt, index) => {
            return (
                <View style={Style.card} key={index}>
                    <Image style={{width: "100%", height: 80, borderRadius: 10}} source={require('../assets/pont.jpg')}></Image>
                    <View style={{paddingHorizontal: 15, paddingVertical: 5, flex: 1}}>
                    <View>
                        <Text style={Style.cardNoteTitle}>{hunt.title}</Text>
                        </View>
                        <View>
                        <Text style={{textAlign:"center"}}>{hunt.description}</Text>
                        </View>
                        <View style={{width: 250, height:50, flexDirection:"row", justifyContent: "space-between", alignItems: "center", marginTop: 30}}>
                            <View style={{flex:1}}>
                            <Image style={{width:30, height:30}} source={require('../assets/star.jpg')}></Image>
                            </View>
                            <View style={{flex:1}}>
                            <Button
                            title = "Go !"
                            onPress = {() => {this.context.selectHunt(hunt._id);
                                            this.props.navigation.navigate("MapScreen")}}
                            style = {Style.cardButton}
                            textStyle = {[Style.textButtonStyle, {fontSize: 10}]}
                            />
                            </View>
                            <View style={{flex:1}}>
                            <Text style={Style.cardNoteTitle}>{hunt.clues.length} lieux a decouvrir</Text>
                            </View>
                        </View>
                    </View>
                </View>
            )
        }) 
        : <Text>PAS DE CHASSE DISPONIBLE ACTUELLEMENT</Text>
    }

    render() {
        return (        
            <ImageBackground source={require('../assets/background.jpg')} imageStyle={Style.backgroundImage}  style={Style.container}>
                <SafeAreaView style={Style.container}>
                    <ScrollView 
                    style={{ flex: 1 }}
                    scrollEnabled={true}
                    >
                        {this._renderHunts()}
                    </ScrollView>
                </SafeAreaView>
            </ImageBackground>
        );
    }
}