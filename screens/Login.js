import React, { useState, useContext }  from 'react';
import { View, ImageBackground, TextInput, Alert, AsyncStorage } from 'react-native';
import { Button } from '../components/Button';
import { Style, inputColor } from '../style/style';
import JWT from "expo-jwt";
import { ProfileContext } from '../components/ProfileProvider';

const key = "secret";

const Login = props => {

    const login = async function(email, password) {
        let res = await getToken(email, password);
        if (res.success === "JWT auth") {
            Alert.alert("CONNEXION RÉUSSIE");
            let token = JWT.decode(res.token, key);
            console.log(token);
            await AsyncStorage.setItem("token", token._id);
            //props.navigation.push("MyAccount");
        }
        // TODO déplacer les erreurs dans un fichier contenant toutes les erreurs
        else if (res.message === "BAD_PASSWORD") {
            // TODO faire un petit modal stylé
        }
        else if (res.message === "EMAIL_NOT_FOUND") {
            // TODO pareil
        }
    }

    const getToken = async function(email, password) {
        const response = await fetch("https://serene-waters-97669.herokuapp.com/api/login", {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                "email": email,
                "password": password
            })
        });
        const json = await response.json();
        return json;
    }

    const getUser = async function(id) {
        const response = await fetch("https://serene-waters-97669.herokuapp.com/api/users/"+id, {
            method: "GET",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
        });
        const json = await response.json();
        return json;
    }

    const [email,setEmail] = useState("");
    const [password, setPassword] = useState("");
    const context = useContext(ProfileContext);

    return ( 
                <ImageBackground source={require('../assets/background.jpg')} imageStyle={Style.backgroundImage}  style={Style.container}>
                    <View>
                        <TextInput
                        placeholder="Adresse e-mail"
                        placeholderTextColor={inputColor}
                        onChangeText={(email) => setEmail(email)}
                        style={Style.input}
                        />
                        <TextInput
                        placeholder="Mot de passe"
                        placeholderTextColor={inputColor}
                        secureTextEntry={true}
                        onChangeText={(password) => setPassword(password)}
                        style={Style.input}
                        />
                        <Button
                        title = "Je me connecte"
                        onPress = {async () => {
                            await login(email, password);
                            const id = await AsyncStorage.getItem("token");
                            let user = await getUser(id);
                            context.setUser(user);
                            props.navigation.navigate("MyAccount");
                        }}
                        style = {[Style.buttonLogin, {marginTop:'5%'}]} 
                        textStyle = {Style.textButtonStyle}
                        />
                </View>
            </ImageBackground>
    );
}

export default Login;