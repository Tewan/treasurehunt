import React, { useContext } from 'react';
import { Text, View, ImageBackground, AsyncStorage, Image } from 'react-native';
import { Button, ButtonIconEdit } from '../components/Button';
import { Style } from '../style/style';
import { ProfileContext } from '../components/ProfileProvider';
import greenColor from '../style/style';


const MyAccount = props => {

    const context = useContext(ProfileContext);

    const _cleanAsyncStorage = async function() {
        await AsyncStorage.removeItem("token");
        props.navigation.navigate("Login");
    }

    return (
    <ImageBackground source={require('../assets/background.jpg')} style={Style.container} imageStyle={Style.backgroundImage}>
        <View style={[Style.card, {paddingHorizontal: "5%", paddingVertical: "10%"}]}>
            <Image source ={require('../assets/profilePic.png')} style={Style.profilePic}></Image>
            <ButtonIconEdit onPress = { () => {}} style={Style.iconButtonAccount}/>
            <Text style={Style.profileTitle}>E-mail</Text>
            <Text>{context.email}</Text>
            <Text style={Style.profileTitle}>Identité</Text>
            <Text>{context.firstname}</Text>
            <Text>{context.lastname}</Text>
            <Text style={Style.profileTitle}>Mot de passe</Text>
            <Text>{context.selectedHunt}</Text>
            <Button title = "Déconnexion" onPress = {async() => await _cleanAsyncStorage()} />
        </View>
    </ImageBackground>
    )
}

export default MyAccount;