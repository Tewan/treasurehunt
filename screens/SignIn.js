import React, { Component } from 'react';
import { View, ImageBackground, TextInput, Alert } from 'react-native';
import DatePicker from 'react-native-datepicker'
import { Button } from '../components/Button';
import { Style, inputColor } from '../style/style'

import { API_USERS } from '../constants/Api';

export default class SignIn extends Component {
    constructor(props) {
        super(props);
        this.state = {
            lastname: '',
            firstname: '',
            birthdate: 'Date de naissance *',
            email: '',
            password: '',
            colorInput: 'white',
            colorEmailInput: 'white',
            paddingBottomContainer: 0,
            showFirstInput: true
        }
    }

    componentDidMount() {
        // Permet d'initialiser le datePicker à la date d'aujourd'hui
        function pad(s) { return (s < 10) ? '0' + s : s; }
        var d = new Date();
        let date = [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('-');
        if (date !== '')
            this.setState({birthdate: date});
    }

    renderFirstInput () {
        if (this.state.showFirstInput) {
            return (
                <TextInput
                placeholder="Nom *"
                onChangeText={(lastname) => this.setState({lastname})}
                placeholderTextColor={inputColor}
                style={Style.input}
                />
            );
        } else {
            return null;
        }
    }

    render() {
        return (
                <ImageBackground source={require('../assets/background.jpg')} imageStyle={Style.backgroundImage}  style={[Style.container, {paddingBottom: this.state.paddingBottomContainer}]}>
                    <View>    
                        {this.renderFirstInput()}
                        <TextInput
                        placeholder="Prénom *"
                        onChangeText={(firstname) => this.setState({firstname})}
                        placeholderTextColor={inputColor}
                        style={Style.input}
                        />
                        <TextInput
                        placeholder="Adresse e-mail *"
                        onChangeText={(email) => this.setState({email})}
                        placeholderTextColor={inputColor}
                        style={[Style.input, {borderBottomColor:this.state.colorEmailInput}]}
                        />
                        <DatePicker
                        style={{width: 300, marginBottom: '10%'}}
                        date={this.state.birthdate}
                        mode="date"
                        placeholder="Date de naissance"
                        format="DD-MM-YYYY"
                        confirmBtnText="Confirmer"
                        cancelBtnText="Annuler"
                        customStyles={{
                            dateIcon: {
                                position: 'absolute',
                                left: 0,
                                top: 4,
                                marginLeft: 0
                            },
                            dateInput: {
                                borderBottomColor: 'white',
                                borderBottomWidth: 1,
                                borderTopWidth: 0,
                                borderLeftWidth: 0,
                                borderRightWidth: 0,
                                alignItems: "flex-start",
                            },
                            dateText:{
                                color: 'white',
                                marginLeft: 35
                            },
                            placeholderText: {
                                color: 'white'
                            }
                        }}
                        onDateChange={(date) => {this.setState({birthdate: date})}}
                        />
                        <TextInput
                        placeholder="Mot de passe *"
                        secureTextEntry={true}
                        onFocus={() => this.setState({paddingBottomContainer:150})}
                        onEndEditing={() => this.setState({paddingBottomContainer:0})}
                        onChangeText={(password) => this.setState({password})}
                        placeholderTextColor={inputColor}
                        style={[Style.input, {borderBottomColor:this.state.colorInput}]}
                        
                        />
                        <TextInput
                        placeholder="Répétez votre mot de passe **"
                        secureTextEntry={true}
                        onFocus={() => this.setState({showFirstInput:false, paddingBottomContainer:250})}
                        onEndEditing={() => this.setState({showFirstInput:true, paddingBottomContainer:0})}
                        onChangeText={(passwordConfirm) => compareTextInputs(this.state.password, passwordConfirm, this)}
                        placeholderTextColor={inputColor}
                        style={[Style.input, {borderBottomColor:this.state.colorInput}]}
                        />
                        <Button
                        title="Je m'inscris"
                        onPress={async () => await signIn(this)}
                        style = {[Style.buttonLogin, {marginTop:'2%'}]} 
                        textStyle = {Style.textButtonStyle}
                        />
                    </View>
                </ImageBackground>
        );
    }
}

async function signIn(component) {
    if (component.state.colorInput != 'green') {
        Alert.alert("Les deux mots de passes ne correspondent pas !");
    }
    else {
        try {
            await fetch(API_USERS, {
                method: "POST",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(component.state)
            });
            component.props.navigation.navigate("Login");
        }
        catch(e) {
            Alert.alert(JSON.stringify(e));
            console.log(JSON.stringify(e));
        }
    }
}

/**
 * Fonction permettant de comparer le mot de passe et le mot de passe à répéter
 * afin de vérifier que les deux champs sont les mêmes.
 * @param {*} password 
 * @param {*} passwordConfirm 
 */
function compareTextInputs(password, passwordConfirm, component) {
    if (passwordConfirm !== "" && password !== "") {
        if (password == passwordConfirm) {
            // Mettre les deux inputs en vert si les mêmes
            component.setState({colorInput:'green'});
        }
        else {
            // Mettre les deux inputs en rouge si pas les mêmes
            component.setState({colorInput:'red'});
        }
    }
}

/**
 * Buggé
 * @param {*} email 
 */
validateEmailFormat = (email, zis) => {
    let regExpEmail = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    if (regExpEmail.test(email) && email !== "") {
        zis.setState({colorEmailInput: 'green'});
        zis.setState({email});
    }
    else {
        zis.setState({colorEmailInput: 'red'});
    }
}