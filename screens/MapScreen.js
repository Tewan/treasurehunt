import React, { Component } from 'react';
import { View, Alert, Button, TouchableOpacity, Text } from 'react-native';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps'
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';
import { getDistance } from 'geolib'

import { API_COURSES } from '../constants/Api'
import { Style } from '../style/style'
import { ProfileContext } from '../components/ProfileProvider'

const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = 0.0421;

export default class MapScreen extends Component {
    static contextType = ProfileContext;

    constructor(props) {
        super(props);

        this._isMounted = false;

        this.state = {
            coordinates: {
                latitude: 0,
                longitude: 0,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421
            },
            markers: []
        }
    }

    componentDidMount() {
        this._isMounted = true;
        this._isMounted && this._getLocationAsync();
        this._isMounted && this.getHunt();
    }

    componentDidUpdate(prevProps, prevState) {
        setTimeout(() => {
            if (prevState != this.state) {
                let latitude = this.state.coordinates.latitude;
                let longitude = this.state.coordinates.longitude;
                let markers = this.state.markers;
                console.log("Latitude : " + latitude);
                console.log("Longitude : " + longitude);
                if ((latitude !== 0 && longitude !== 0) || (latitude !== undefined && longitude !== undefined)) {
                    console.log("Latitude 2ème barrière passée : " + latitude);
                    console.log("Longitude 2ème barrière passée : " + longitude);
                    if (markers !== undefined) {
                        if (markers.length > 0) {
                            /*for (i= 0; i< this.state.markers.length; i++) {
                                let distance = getDistance({latitude, longitude}, {
                                        latitude: this.state.markers[i].latitude,
                                        longitude: this.state.markers[i].longitude
                                    });
                                if (distance > 50) {
                                    this.setState(prevState => ({
                                        markers: {
                                            ...prevState.markers,
                                            [prevState.markers[i].checked]: true,
                                        },
                                    }));
                                }
                            }*/
                        }
                    }
                }
            }
        }, 2000);
        
        /*
            */
    }

    componentWillUnmount() {
        this._isMounted = false;
        navigator.geolocation.clearWatch(this.watchID);
    }

    render() {
        return (
            <View style = {Style.container}>
                <MapView
                style={Style.map}
                provider={PROVIDER_GOOGLE}
                showsUserLocation={true}
                followsUserLocation={true}
                region={this.getMapRegion()}
                >
                    {this.state.markers.length > 0 ? this.renderMarkers() : null}
                </MapView>
                <TouchableOpacity
                style={{flex: 1,
                    flexDirection:'row',
                    position:'absolute',
                    bottom:10,
                    alignSelf: "center",
                    justifyContent: "space-between",
                    backgroundColor: "transparent",
                    borderWidth: 0.5,
                    borderRadius: 20}}
                onPress={() => {
                    this.context.removeHuntId();
                    this.props.navigation.navigate("HuntsScreen");
                    }}>
                    <Text>Quitter la course</Text>
                </TouchableOpacity>
            </View>
        );
    }

    renderMarkers() {
        return this.state.markers.map((marker, index) => {
            return (
                <Marker
                pinColor= {marker.checked == true ? 'green' : 'red'}
                key={index}
                coordinate={marker.coords}
                />
            );
        })
    }

    _getLocationAsync = async () => {
        let { status } = await Permissions.askAsync(Permissions.LOCATION);
        if (status !== 'granted') {
            this.setState({
                errorMessage: 'Permission to access location was denied',
            });
        }
    
        let location = await Location.getCurrentPositionAsync({});
        let coordinates = {...this.state.coordinates};
        coordinates.longitude = location["coords"]["longitude"];
        coordinates.latitude = location["coords"]["latitude"]
        this._isMounted && this.setState({coordinates});

        if (this._isMounted && this.state.coordinates.latitude != 0 
            && this.state.coordinates.longitude != 0) {
                this.watchID = navigator.geolocation.watchPosition(
                    position => {
                        const { latitude, longitude } = position.coords;
                        const newCoordinate = { latitude, longitude };
                        console.log("Nouvelle position ");
                        console.log(newCoordinate);
                        let newCoordinates = {...this.state.coordinates};
                        newCoordinates.longitude = newCoordinate.longitude;
                        newCoordinates.latitude = newCoordinate.latitude;
                        this._isMounted && this.setState({coordinates: newCoordinates});
                    },
                    error => console.log(error),
                    {
                        enableHighAccuracy: true,
                        timeout: 20000,
                        maximumAge: 1000,
                        distanceFilter: 10
                    }
                )
        }
    };

    getMapRegion = () => ({
        latitude: this.state.coordinates.latitude,
        longitude: this.state.coordinates.longitude,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
    });

    getHunt = async () => {
        const response = await fetch(API_COURSES +"/5dc3df8f882b4d0004531e0c", {
            method: "GET",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            }
        });
        const json = await response.json();
        if (json.clues.length > 0) {
            let markers = json.clues.map((clue) => ({
                description: clue.description,
                coords: {
                    latitude: clue.coords.latitude,
                    longitude: clue.coords.longitude
                },
                checked: false
            }));
            this._isMounted && this.setState({markers});
        }
    }
}