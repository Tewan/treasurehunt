import React, {useContext, useEffect, useState} from 'react';
import { Text, View, ImageBackground, Alert, AsyncStorage } from 'react-native';
import { Button } from '../components/Button';
import { Style } from '../style/style';
import { ProfileContext } from '../components/ProfileProvider';


const Home = (props) => {

    const context = useContext(ProfileContext);
    const [isLoading, setIsLoading] = useState(false);

    const init = async function() {
        setIsLoading(true);
        const id = await AsyncStorage.getItem("token"); 
        if (id != null) {
            let user = await getUser(id);
            context.setUser(user);                    
            props.navigation.navigate("HuntsScreen");
        }
    }

    const getUser = async function(id) {
        const response = await fetch("https://serene-waters-97669.herokuapp.com/api/users/"+id, {
            method: "GET",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
        });
        const json = await response.json();
        return json;
    }

    useEffect( () => {
        if(!isLoading)
            init();            
    }) 

    return (
        <ImageBackground source={require('../assets/backgroundHome.jpg')} style={Style.container} imageStyle={Style.backgroundImage}>
            <View style={Style.buttonGroup}>
                <Button
                title = "Connexion"
                onPress = {() => {props.navigation.navigate('Login')}}
                style = {Style.buttonLogin}
                textStyle = {Style.textButtonStyle}
                />
                <Button
                title = "Pas encore inscrit ?"
                onPress = {() => {props.navigation.navigate('SignIn')}}
                textStyle = {Style.registerButtonStyle}
                />
            </View>
        </ImageBackground>
    )
}

export default  Home;